//
//  Manager.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import Foundation


class ManagerFavourite {
    let cacher :  Cacher<[MovieModel]>
    
    var savedFavouriteMovie: [MovieModel] {
        cacher.value(forKey: .favourite) ?? []
    }
    
    
    init() {
        cacher =  Cacher<[MovieModel]>()
    }
    
    func addFavouriteMovie(movie: MovieModel) -> (success: Bool, msg: String) {
        var  favourite  = savedFavouriteMovie
        
        if favourite.contains(where: {$0.id == movie.id }) {
            return (success: false, msg: "Already saved")
        }  else {
            favourite.append(movie)
            let success = cacher.setValue(favourite, key: .favourite)
            let msg = success ? "Successfully add to favourite list" : "Can't add to favourite list. Please try again"
            return (success: success, msg: msg)
        }
    }
}


class MovieManager {
    let cacher :  Cacher<[MovieModel]>
    
    var cachedMovie: [MovieModel] {
        cacher.value(forKey: .newMovie) ?? []
    }
    
    init() {
        cacher =  Cacher<[MovieModel]>()
    }
    
    
    func addMovie(movie: MovieModel) -> (success: Bool, msg: String) {
        var  savedMovie  = self.cachedMovie

        if savedMovie.contains(where: {$0.id == movie.id }) {
            return (success: false, msg: "Already saved")
        }  else {
            savedMovie.append(movie)
            let success = cacher.setValue(savedMovie, key: .newMovie)
            let msg = success ? "Successfully add to new movie in list" : "Can't add  movie. Please try again"
            return (success: success, msg: msg)
        }
    }
    
    func getMovie() -> [MovieModel] {
        var movies = [MovieModel]()
        if let jsonFile = Bundle.main.path(forResource: "MovieFile", ofType: "json"),
           let data  = try? Data(contentsOf: URL(fileURLWithPath: jsonFile)),
           let moviesData = try? JSONDecoder().decode([MovieModel].self, from: data) {
            movies = moviesData
        }
        movies += cachedMovie
        return movies
    }
}
