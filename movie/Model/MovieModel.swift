//
//  MovieModel.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import Foundation


enum MovieType: String, CaseIterable {
    case action = "Action"
    case sciFi = "Sci-Fi"
    case comedy  =  "Comedy"
}

struct MovieModel: Codable {
    var id: String = "mov_" + UUID().uuidString
    var name: String = ""
    var cast: String = ""
    var type: String = ""
    var imageName: String = ""
    var thumbnailName: String = ""
    var imageDataString: String = ""
    
    init() {}
    
    init(from decoder: Decoder) throws {
        let cointainer = try decoder.container(keyedBy: CodingKeys.self)
        id  = try cointainer.decode(String.self, forKey: .id)
        name  = try cointainer.decodeIfPresent(String.self, forKey: .name) ?? ""
        cast  = try cointainer.decodeIfPresent(String.self, forKey: .cast) ?? ""
        type  = try cointainer.decodeIfPresent(String.self, forKey: .type) ?? ""
        imageName  = try cointainer.decodeIfPresent(String.self, forKey: .imageName) ?? ""
        thumbnailName  = try cointainer.decodeIfPresent(String.self, forKey: .thumbnailName) ?? ""
        imageDataString  = try cointainer.decodeIfPresent(String.self, forKey: .imageDataString) ?? ""
    }
    
    
    var castline: String {
        cast.replacingOccurrences(of: ",", with: "\n")
    }
}
