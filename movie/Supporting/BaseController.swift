//
//  BaseController.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import UIKit

class BaseController: UIViewController {

    static var identifier: String {
        "\(self)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func alertMessage(message: String ) {
        let alert  = UIAlertController(title: "Movies App" , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func alertWithOkHandler(message: String, handler: @escaping (() -> ()) ){
        let alert  = UIAlertController(title: "Movies App" , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            handler()
        }))
        present(alert, animated: true, completion: nil)
    }

}
