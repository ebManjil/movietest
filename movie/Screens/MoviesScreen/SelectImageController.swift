//
//  SelectImageController.swift
//  movie
//
//  Created by manjil on 03/02/2021.
//

import UIKit

class SelectImageController: BaseController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let images = ["play", "play.fill", "film.fill", "film", "face.smiling.fill", "face.smiling", "car", "car.fill","arrow.3.trianglepath"]
    
    var selectImage: ((String ) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollection()
        collectionView.reloadData()
        // Do any additional setup after loading the view.
    }

}


extension SelectImageController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private func setupCollection() {
        collectionView.register(UINib(nibName: ImageCell.idenifier, bundle: nil), forCellWithReuseIdentifier: ImageCell.idenifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.idenifier, for: indexPath) as! ImageCell
        
        cell.image = images[indexPath.item]
        
       return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selected = selectImage {
            selected(images[indexPath.item])
            navigationController?.popViewController(animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame
        let width = size.width / 4
        return CGSize(width: width, height: width)
    }
    
    
}
