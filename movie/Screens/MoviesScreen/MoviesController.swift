//
//  MoviesController.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import UIKit

class MoviesController: BaseController {
    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    // MARK: - parameteras
    var movie = [MovieModel]()
    var selectMovie = [MovieModel]()
    let favouriteManager = ManagerFavourite()
    let movieManager = MovieManager()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControl.selectedSegmentIndex  = 0
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
}

// MARK: - Actions
extension MoviesController {
    @IBAction func segmentControlTouched(_ sender: UISegmentedControl) {
        loadMovie()
    }
}

// MARK: - TableView Delegate
extension MoviesController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: MovieCell.idenifier, bundle: nil), forCellReuseIdentifier: MovieCell.idenifier)
        tableView.tableFooterView = UIView()
        
        let doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(sender:)))
            doubleTapGestureRecognizer.numberOfTapsRequired = 2
            tableView.addGestureRecognizer(doubleTapGestureRecognizer)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(sender:)))
            tapGestureRecognizer.numberOfTapsRequired = 1
            tapGestureRecognizer.require(toFail: doubleTapGestureRecognizer)
            tableView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        selectMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.idenifier, for: indexPath) as! MovieCell
        cell.movie = selectMovie[indexPath.row]
        return cell
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }

    @objc func handleTapGesture(sender: UITapGestureRecognizer) {
        let touchPoint = sender.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
           gotoDetail(index: indexPath)
        }
    }

    @objc func handleDoubleTap(sender: UITapGestureRecognizer) {
        let touchPoint = sender.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: touchPoint) {
            addFavourite(index: indexPath)
        }
    }
    
}
// MARK: - Methods
extension MoviesController {
    private func getData() {
        movie = movieManager.getMovie()
        loadMovie()
    }
    
    private func loadMovie() {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            selectMovie = movie
        case 1:
            selectMovie  = movie.filter({$0.type.lowercased() == MovieType.action.rawValue.lowercased() })
        case 2:
            selectMovie  = movie.filter({$0.type.lowercased() == MovieType.sciFi.rawValue.lowercased() })
        case 3:
            selectMovie  = movie.filter({$0.type.lowercased() == MovieType.comedy.rawValue.lowercased() })
        default:
            break
        }
        tableView.reloadData()
    }
    
    
    private func addFavourite(index: IndexPath) {
        let movie = selectMovie[index.row]
        let response = favouriteManager.addFavouriteMovie(movie: movie)
        alertMessage(message: response.msg)
    }
    
    
    private func gotoDetail(index: IndexPath) {
        let movie = selectMovie[index.row]
        
        let storybord  = UIStoryboard(name: "Main", bundle: nil)
        if  let controller = storybord.instantiateViewController(identifier: MovieDetailController.identifier) as? MovieDetailController {
            controller.movie = movie
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    
    private func testforMe() {
   
    }
}
 private  func testforMe2() {

 }

