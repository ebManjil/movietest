//
//  NewMovieController.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import UIKit

class NewMovieController: BaseController {
    
    // MARK: - outlet
    @IBOutlet weak var movieNameText: UITextField!
    @IBOutlet weak var castTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var genereText: UITextField!
    
    
    @IBOutlet weak var label: UILabel!
    // MARK: - parameters
    let manager = MovieManager()
    private var newMovie = MovieModel()
    let generes = MovieType.allCases
    let picker = UIPickerView()
    let imagePicker = UIImagePickerController()
    
    // MARK: - cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        castTextView.layer.borderWidth = 1
        imageView.layer.borderWidth = 1
        castTextView.layer.borderColor = UIColor.gray.cgColor
        imageView.layer.borderColor = UIColor.gray.cgColor
        movieNameText.layer.borderWidth = 1
        movieNameText.layer.borderColor = UIColor.gray.cgColor
        newMovie.type = generes[0].rawValue
        setUpPicker()
        genereText.inputView = picker
        picker.selectRow(0, inComponent: 0, animated: false)
        genereText.text = generes[0].rawValue
        setUpPicker()
        
    }
}

// MARK: - Action
extension NewMovieController {
    @IBAction func cancelButtonTouched(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonTouched(_ sender: UIBarButtonItem) {
        validateAndSave()
    }
    
    @IBAction func selecImageButtonTouched(_ sender: UIButton) {
        actionSheet()
    }
    
    
}
// MARK: - methods
extension NewMovieController {
    private func validateAndSave() {
        if  movieNameText.text?.isEmpty  ?? true {
            alertMessage(message: "Please write movie name")
            return
        } else if castTextView.text.isEmpty {
            alertMessage(message: "Please write cast names")
            return
        }
        newMovie.name = movieNameText.text ?? ""
        newMovie.cast = castTextView.text
        let response = manager.addMovie(movie:  newMovie)
        
        if response.success {
            alertWithOkHandler(message: response.msg) { [weak self] in
                guard let self = self else { return }
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            alertMessage(message: response.msg)
        }
    }
    
    private func fileTester() {
        
    }
}
// MARK: - Picker delegate
extension NewMovieController: UIPickerViewDelegate, UIPickerViewDataSource {
    private func setUpPicker() {
        picker.delegate = self
        picker.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        generes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        generes[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genereText.text = generes[row].rawValue
        newMovie.type = generes[row].rawValue
    }
    
}


// MARK: - image picker
extension NewMovieController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private func setImagePicker() {
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
    }
    
    
    private func actionSheet() {
        let sheet = UIAlertController(title: "Movie image", message: "", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "camera", style: .default, handler: {[weak self] (_) in
            self?.openImagePicker(type: .camera)
        }))
        sheet.addAction(UIAlertAction(title: "photo", style: .default, handler: {[weak self] (_) in
            self?.openImagePicker(type: .photoLibrary)
        }))
        
        sheet.addAction(UIAlertAction(title: "app", style: .default, handler: {[weak self] (_) in
            self?.appImage()
        }))
        
        
        present(sheet, animated: true, completion: nil)
    }
    
    
    private func openImagePicker(type: UIImagePickerController.SourceType) {
        imagePicker.delegate = self
        if !UIImagePickerController.isSourceTypeAvailable(type) {
            alertMessage(message: "not working")
            return
        }
        imagePicker.sourceType = type
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            
            
            //let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            
            // create and send the Image structure
            self.imageView.image = editedImage ?? originalImage
            self.newMovie.imageDataString = editedImage?.pngData()?.base64EncodedString() ?? originalImage?.pngData()?.base64EncodedString() ?? ""
            
            
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    private func appImage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let viewController = storyboard.instantiateViewController(identifier: "SelectImageController") as? SelectImageController {
            viewController.selectImage = { [weak self]  image in
                guard let self = self else { return }
                self.imageView.image = UIImage(systemName: image)
                self.newMovie.imageName =  image
                self.newMovie.thumbnailName = image
            }
            navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
}

