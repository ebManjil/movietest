//
//  FavouriteController.swift
//  movie
//
//  Created by manjil on 31/01/2021.
//

import UIKit

class FavouriteController: BaseController {

    // MARK: - outlet 
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - parameter
    var movie = [MovieModel]()
    let favouriteManager = ManagerFavourite()
    
    var testPrint: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
    
    private func uslessfile() {
    
    }
    
//    public func usless() {  }
}

extension FavouriteController: UITableViewDelegate, UITableViewDataSource {
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: MovieCell.idenifier, bundle: nil), forCellReuseIdentifier: MovieCell.idenifier)
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        movie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.idenifier, for: indexPath) as! MovieCell
        cell.movie = movie[indexPath.row]
        return cell
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        gotoDetail(index: indexPath)
    }
}

// MARK: - Methods
extension FavouriteController {
    private func getData() {
        movie = favouriteManager.savedFavouriteMovie
        tableView.reloadData()
       
    }
    
    private func gotoDetail(index: IndexPath) {
        let movie = self.movie[index.row]
        let storybord  = UIStoryboard(name: "Main", bundle: nil)
        if  let controller = storybord.instantiateViewController(identifier: MovieDetailController.identifier) as? MovieDetailController {
            controller.movie = movie
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}
