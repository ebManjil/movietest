//
//  ImageCell.swift
//  movie
//
//  Created by manjil on 03/02/2021.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!

    var image: String! {
        didSet {
            movieImageView.image = UIImage(systemName: image)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
