//
//  MovieDetailController.swift
//  movie
//
//  Created by manjil on 31/01/2021.
//

import UIKit

class MovieDetailController: BaseController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var castNameLabel: UILabel!
    
    var movie: MovieModel!
    override func viewDidLoad() {
        super.viewDidLoad()
         title = ""
        if let data = Data(base64Encoded: movie.imageDataString), let image = UIImage(data: data) {
            imageView.image = image
        } else if let image = UIImage(named: movie.imageName) {
            imageView.image = image
        } else {
            imageView.image = movie.imageName.isEmpty ?  UIImage(systemName: "film.fill") : UIImage(systemName: movie.imageName)
        }
        movieNameLabel.text = movie.name
        castNameLabel.text = movie.castline
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
