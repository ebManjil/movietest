//
//  MovieCell.swift
//  movie
//
//  Created by manjil on 30/01/2021.
//

import UIKit

class MovieCell: UITableViewCell {
    // MARK: - outlet
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var castLabel: UILabel!
   
    var testSting: String = ""
    // MARK: - parameters
    var movie: MovieModel! {
        didSet {
            configuration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    // MARK: - methods
    private func configuration() {
        titleLabel.text = movie.name
        castLabel.text = movie.cast
        
        if let data = Data(base64Encoded: movie.imageDataString),  let image = UIImage(data: data) {
            movieImageView.image = image
            return
            
        }
        
        if let image =  UIImage(named: movie.thumbnailName) {
            movieImageView.image = image
        } else {
            movieImageView.image = movie.thumbnailName.isEmpty ? UIImage(systemName: "film.fill") : UIImage(systemName: movie.thumbnailName)
        }
       
    }
}
